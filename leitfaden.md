# Define Leitfaden

## Kurzfassung

Projekt Define macht die Maker-Bewegung für blinde und sehbehinderte Menschen zugänglich.
Dieser Organisationsleitfaden basiert auf unserem Vorgehen, unseren Erfahrungen und Anregungen für nachfolgende Projekte.

## Einführung

Der Zugang zu Technologie ist nicht gleichmäßig verteilt.
Behinderte Menschen stoßen immer noch auf erhebliche Barrieren. 

Menschen mit Behinderung, speziell Schüler:innen und Jugendliche mit Behinderung, die noch am Anfang ihres Arbeitslebens stehen, brauchen eine spezielle Vermittlung von digitalen Kompetenzen und individuelle Förderung, damit sie am Arbeitsmarkt partizipieren können. 

Das bedeutet auch, dass die Integration von Menschen mit Behinderung am Arbeitsmarkt bereits bei der Produktion und Entwicklung von Technologien berücksichtigt werden muss. Nachträgliche Erweiterungen oder Zugangsmöglichkeiten zu technischen Programmen sind oft nur schwer oder gar nicht mehr möglich. Daher sollten Software, Hardware und Programme auch direkt von Betroffenen gestaltet werden – nur so kann echte Teilhabe aussehen und garantiert werden, dass Technologien tatsächlich von allen nutzbar sind.

Die Macher-Bewegung (Maker-Bewegung) ermöglicht mit einer wachsenden Zahl lokaler, offener Werkstätten mit digitaler Fertigungstechnologie (MakerSpaces) einen breiten Zugang zu digitaler, angepasster Technologie und Innovation.

Im Rahmen des Projekts „Define“ (= Digital Equipment for INclusive Empowerment) hat der Blinden- und Sehbehindertenverband eine Workshopreihe konzipiert, die Jugendliche mit Behinderung an Digitalisierung teilhaben lassen: Konkret wurde eine Bluetooth-Braille-Tastatur für PC und Smartphone entwickelt und selbst von den blinden und sehbehinderten Workshop-Teilnehmer:innen angefertigt.

Bei der Arbeit in offenen Werkstätten wurden Barrieren überwunden und inklusive Zugänge zu den digitalen Fertigungstechniken der Maker-Bewegung entwickelt.

## Infrastruktur ##

### Hardware ###

#### Arbeitsplatz ####

Als physische Arbeitsplätze haben wir die offenen Werkstätten [metalab](https://metalab.at/) und [Happylab](https://happylab.at/) sowie die Werkstätten des [Bundes-Blindenerziehungsinstitut (BBI)](https://bbi.at/) nutzen können.

#### Telefon ####

Für das Projekt haben wir ein Smartphone besorgt. Damit war das Projekt unter einer einzelnen Telefonnummer erreichbar und wir konnten das Smartphone für die Dokumentation des Projekts nutzen.

#### Arbeitsmaterial ####

Für Brainstorming und Gestaltung in der Phase 2 Einführung haben wir Klemmbausteine, Knetmasse und Draht besorgt.

### Webspace Hosting, Domain und E-Mail ###

Webspace Hosting, Domain und E-Mail haben wir bei [MyRoot.PW](https://myroot.pw/) eingekauft.

### Software ###

Bei der Auswahl unserer Software haben wir [Open-Source-Software](https://de.wikipedia.org/wiki/Open_Source) gegenüber [proprietärer Software](https://de.wikipedia.org/wiki/Propriet%C3%A4re_Software) bevorzugt. Wir hatten sehr unterschiedliche Zugänge, Vorkenntnisse und Vorlieben, welche die Auswahl von gemeinsam genutzter Software erschwerte.

#### Website ####

Zur Erstellung unserer Website mit Blog haben wir den [Static site generator](https://en.wikipedia.org/wiki/Static_site_generator) [Hugo](https://en.wikipedia.org/wiki/Hugo_(software)) verwendet. Für Webanalytik wurde [Matomo](https://de.wikipedia.org/wiki/Matomo) eingesetzt.

#### Online Kollaboration Software ####

Zu Anfangs haben wir "Microsoft Teams (abgekürzt „Teams“ oder „MS Teams“) eine von Microsoft entwickelte Plattform, die Chat, Teams, Besprechungen, Notizen und Anhänge kombiniert", siehe [Wikipedia Microsoft Teams](https://de.wikipedia.org/wiki/Microsoft_Teams), verwendet. Wir sind fälschlicherweise davon ausgegangen, dass die meisten unsere Teilnehmer:innen mit dieser Plattform vertraut sind. Während der Projektlaufzeit hat Microsoft die kostenlose Organisation von Teams eingestellt. Da Microsoft Teams unsere Erwartung nicht erfüllen konnte, haben wir den Betrieb dort eingestellt. 

Für den Datenaustausch haben wir [Nextcloud](https://de.wikipedia.org/wiki/Nextcloud) verwendet. Weitere Funktionen wie geteilte Kalender und [Online-Office](https://de.wikipedia.org/wiki/Online-Office) halfen bei der Verwaltung des Projekts, wurden aber von den Teilnehmer:innen sehr viel weniger benutzt.

Die für uns wahrscheinlich wichtigste Online Kollaboration Software stellt das [Webkonferenzsystem](https://de.wikipedia.org/wiki/Webkonferenz) [BigBlueButton](https://de.wikipedia.org/wiki/Webkonferenz) dar, da wir es für wöchentliche online Besprechungen nutzten. Wir haben die [BigBlueButton Instanz vom metalab](https://bbb.metalab.at) genutzt. Leider hatten einzelne Teilnehmer:innen häufige Verbindungsprobleme vom Smartphone zur Webkonferenz Instanz, welche wir nicht beheben konnten.

#### Instan Messaging ####

Wir haben den von Sicherheitsexperten empfohlenen Open-Source [Nachrichtensofortversand](https://de.wikipedia.org/wiki/Instant_Messaging) [Signal](https://de.wikipedia.org/wiki/Signal_(Messenger)) für das Projekt gewählt.
Obwohl [WhatsApp](https://de.wikipedia.org/wiki/WhatsApp) weit verbreitet und für blinde und sehbehinderte Menschen einfach zu bedienenden ist, haben wir uns vor allem aus [datenschutzrechtlichen Gründen](https://de.wikipedia.org/wiki/WhatsApp#Datenschutz) gegen diesen Nachrichtensofortversand entschieden.

#### Kollaboratives Schreiben ####

Zum [kollaborativen schreiben](https://de.wikipedia.org/wiki/Kollaboratives_Schreiben) haben wir [Etherpad](https://de.wikipedia.org/wiki/Etherpad) und [CryptPad](https://cryptpad.fr/) verwendet. Diese Arbeitsform wurde anfangs von den Teilnehmer:innen begeistert aufgenommen, es wurde aber auch bald die springende Eingabeaufforderung beim [Bildschirmvorleseprogramm (Screenreader)](https://de.wikipedia.org/wiki/Screenreader) bemängelt.

#### Entwicklungsumgebung ####

- [PlatformIO](https://de.wikipedia.org/wiki/PlatformIO)
- [Python](https://de.wikipedia.org/wiki/Python_(Programmiersprache))
- [Arduino_IDE](https://de.wikipedia.org/wiki/Arduino_IDE)

#### CAD ####

[OpenSCAD](https://de.wikipedia.org/wiki/OpenSCAD)

#### Slicer ####

[Slicer-Software](https://de.wikipedia.org/wiki/Slicer-Software)

- [Ultimaker_Cura](https://de.wikipedia.org/wiki/Ultimaker_Cura)
- [PrusaSlicer](https://www.prusa3d.com/page/prusaslicer_424/)

#### Repositorien ####

- [GitLab](https://de.wikipedia.org/wiki/GitLab) ist ein Onlinedienst zur Softwareentwicklung und Versionsverwaltung für Softwareprojekte auf Git-Basis.
  - [gitlab.com](https://gitlab.com/)
  - [gitlab.opensourceecology.de](https://gitlab.opensourceecology.de/)
- [Thingiverse](https://de.wikipedia.org/wiki/Thingiverse)
- [LOSH](https://losh.opennext.eu/)

## Phase 1 Vorbereitung
### Offener Aufruf
Information ist die Voraussetzung um Prozesse mitgestalten zu können.
In einem offenen Aufruf wurden blinde und sehbehinderte Menschen, die Maker-Bewegung, Kooperationspartner und die Zivilgesellschaft informiert und zur Teilnahme eingeladen.

Die Kooperationspartner haben sich, durch Zusammenarbeit vor Projektbeginn, in die Gestaltung des Projekts eingebracht.

### Datenschutz
Zum Schutz der privaten Daten aller Teilnehmer:innen am Projekt haben wir eine Datenschutzvereinbarung und eine Einverständniserklärung erstellt. Darin wurden der Umfang der erhobenen privaten Daten und der Verwendungszweck erklärt. Mit der Einverständniserklärung haben wir die Dokumentation des Projekts und veröffentlichung von Aufnahmen zur unter der CC BY-SA 4.0 Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz ermöglicht.

### Mobilität ###
Die Mobilität der einzelnen Teilnehmer:innen muss im Vorfeld abgeklärt werden. Wie können die Teilnehmer:innen zu den physischen Treffen gelangen? Wo befinden sich gemeinsam nutzbare Treffpunkte, um gemeinsam in die Werkstätten zu kommen?

### Termine ###
Wann können sich die Teilnehmer:innen gemeinsam treffen?

### Abstimmung ###
Wie werden Entscheidungen innerhalb des Projekts getroffen?
Wir haben für wichtige Entscheidungen [systematisches Konsensieren](https://gruppenentscheidung.de/) eingesetzt.
Unsere Abstimmungen haben wir in textbasierte Listen zuerst über [kollaborativen schreiben](https://de.wikipedia.org/wiki/Kollaboratives_Schreiben) dann über [Online-Office](https://de.wikipedia.org/wiki/Online-Office) abgewickelt. Unsere Abstimmungen über online zugängliche Dokumente konnten zwar nicht anonym durchgeführt werden und waren nicht vor Vandalismus geschützt, das hat für uns aber kein Problem dargestellt. Im Unterschied zur Abstimmung mit Web-Formularen ist die Erstellung von textbasierte Listen in online zugänglichen Dokumenten für die Teilnehmer:innen selbst auch zugänglich.


## Phase 2 Einführung
### Themen
- Projektvorstellung  
  Zu Beginn sollte eine Übersicht über das Projekt geboten werden damit alle dieselben Erwartungen haben.
  Ziele, Umfang und Rahmen/Grenzen des Projekts sind die wichtigsten Ausgangspunkte.
- Vorstellung Teilnehmer:innen  
  Die gegenseitige Vorstellung bildet den ersten Schritt zum gegenseitigen Vertrauen.
  Beim Projekt Define kannten sich die Teilnehmer:innen schon vor dem Projekt.
- Verhaltensregeln  
  Die Durchsetzung von respektvollem Umgang schafft ein sicheres Arbeitsumfeld.
  Beim Projekt Define kannten sich die Teilnehmer:innen schon vor dem Projekt.
  Beim Projekt Define wurde auch der Schutz von privaten Daten und die Veröffentlichung von Aufnahmen und Ergebnissen unter einer offenen Lizenz in Form einer Einverständniserklärung behandelt.
- Rollen im Projekt  
  Im Projekt werden Rollen und zugehörige Aufgaben definiert und verteilt. Neben der Rolle/Aufgabe Moderation/Koordination sind Expertenwissen, Technik und Dokumentation gut anwendbar.
  Beim Projekt Define waren die Gruppen zu klein um die Aufgaben und Rollen auf einzelne Personen aufteilen zu können.
- Mitgestaltungskonzepte  
  Beim Projekt Define wurden Partizipatives Gestalten (Co-create), Design Thinking und systemisches Konsensieren vorgestellt.
- Makerbewegung und Open Source  
  Bei uns wurden die für das Projekt wichtige Makerbewegung und die Open Source Lizenzen vorgestellt.
- Dokumentation  
  Die Dokumentation des Projektfortschritts und der Projektergebnisse sind wichtig für die Kommunikation nach innen und nach außen.
  Für die Dokumentation ist genügend Zeit einzuplanen.
- Konzeptentwicklung  
  Vielfälltige Ideen und Lösungen ohne Beschränkung in einem Brainstorm sammeln.
	  - Vorbereitete Fragen
		  - Welche Schwierigkeiten fallen dir auf?
		  - Was können wir?
		  - Was wollen wir können?
		  - Wie sollen wir zusammenarbeiten?
	  - 10 Ideen sammeln
	  - Top 5 auswählen
- Erforschung der Herausforderung
  Sicherstellung der Identifizierung der wichtigsten Aspekte der Herausforderung mit Endbenutzer im Hinterkopf als Leitlinie für den Gestaltungsprozess und die Herstellung des Prototypen.

### Workshops
Die Workshops werden am besten schon im zukünftigen Arbeitsumfeld abgehalten.
Projekt Define hat die Einführung in zwei Gruppen im [Bundes-Blindeninstituts Wien (BBI)](https://bbi.at), [Happylab](https://happylab.at/de_vie/) und [metlab](https://metalab) zu je drei Workshops abgehalten.

Beim Projekt Define konnte nicht auf nicht bewährtes visuelles Brainstorming Materialien wie (Post-its, Papier, Marker, Schere, Klebeband) zurückgegriffen werden. Für die Gestaltung und Kommunikation von Ideen haben wir Knetmasse, Draht und Klemmbausteine verwendet. Weiters haben wir Sprachaufzeichnungen angefertigt und Inhalte durch Wiederholung strukturiert.

In einem öffentlichen Workshop und in online Besprechungen wurde die Zivilgesellschaft zur Mitgestaltung eingeladen.

## Phase 3 Prototypen-Serie
### Themen
### Workshops
## Abschließende allgemeine Hinweise
## Urheberrecht & Nutzungsrechte
Define Leitfaden © 2023 von BSV-WNB/Define kann wiederverwendet werden unter der CC BY-SA 4.0 Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz, abrufbar unter folgender Adresse <https://creativecommons.org/licenses/by-sa/4.0/deed.de> .
## Impressum
Projekt Define 

Blinden- und Sehbehindertenverband Wien, Niederösterreich und Burgenland

Selbsthilfeorganisation

Eingetragener Verein mit Sitz in Wien - ZVR-Zahl: 222700859

Gemeinnützige Interessenvertretung blinder und sehbehinderter Menschen

Hägelingasse 4-6

1140 Wien
